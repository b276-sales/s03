class Camper():
	
	#Initialize
	def __init__(self, attribute_name, batch, course_type):
		self.attribute_name = attribute_name
		self.batch = batch
		self.course_type = course_type

	# Method
	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program")

	def info(self):
		print(f"My name is {self.attribute_name} of batch {self.batch}")

new_camper = Camper("Alan", 100, "python short course")

print(f"Camper Name: {new_camper.attribute_name}")
print(f"Camper Batch: {new_camper.batch}")
print(f"Camper Course: {new_camper.course_type}")

new_camper.info()

new_camper.career_track()

